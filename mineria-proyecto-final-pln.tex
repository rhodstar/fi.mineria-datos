\documentclass[twocolumn,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage[total={17.3cm,23cm},top=1.5cm,left=2cm]{geometry}

\usepackage{graphicx}
\usepackage{fancyhdr} 
\usepackage{titlesec}
\usepackage{longtable}
\usepackage{array}                    % Align fix size columns in tables
\usepackage[export]{adjustbox}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{float}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{url}

\decimalpoint%
\setlength{\parindent}{0in}
\setlength{\parskip}{3mm}
\setlength{\columnsep}{8mm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%                        Code style                         %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{1,1,1}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{upquote=true,style=mystyle}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%              Cover page generator command                 %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{Generación de un modelo del lenguaje natural utilizando redes neuronales}
\author{Rodrigo Francisco Pablo \\[3mm] Facultad de Ingeniería}
\date{}

\graphicspath{ {latex/assets/} {assets/} }
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%                        Contents                           %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract}
  El procesamiento del lenguaje natural es una de las ramas de la inteligencia
  más estudiada recientemente, ya que su entendimiento permite la construcción
  de entes de software como traductores automáticos, correctores ortográficos,
  el reconocimiento óptico de caracteres, el reconocimiento de escritura, entre
  otros. Sin embargo, abordar el problema no es trivial debido a que, pese que
  el lenguaje natural tenga algunas reglas y heurísticas, también es cierto, que
  es demasiado permisivo y por ende tienden a generarse ambigüedades. El
  presente trabajo pretende abordar la creación de un modelo del lenguaje
  natural que sea capaz de ser flexible y preciso al mismo tiempo, por tal
  motivo se aborda el problema desde una perspectiva estadística, por medio
  del uso de redes neuronales, en la cual se ocupan como entradas bigramas de la
  forma $(w_i,w_j)$, construidos apartir de un corpus en español.
\end{abstract}

\section*{Introducción}

La generación de modelos del lenguaje es un de las tareas centrales más
importantes del procesamiento del lenguaje natural (PLN o NLP, por sus siglas en
inglés), debido a que estos se usan para generar texto y por ende aplicaciones
como:

\begin{itemize}
  \item Reconocimiento óptico de caracteres (OCR)
  \item Reconocimiento de escritura
  \item Traducción automática
  \item Corrección ortográfica
  \item Resúmenes de texto
\end{itemize}

Sin embargo, crear un modelo que no es tarea sencilla ya que el lenguaje natural
tiene ciertas reglas y heurísticas que no necesariamente se siguen al pie de la
letra en una conversación entre parlantes del mismo idioma.
El lenguaje natural involucra un vasto número de términos que pueden ser usados
en maneras que introduzcan ambigüedades que la computadora no sepa como
entender.
Otro problema surge a la hora de modelar el lenguaje es que los idiomas cambian
a través del tiempo, así como también el uso que se le da a las palabras.

Para la generación de modelos del lenguaje se tienen dos enfoques:
\textit{perspectivas formales}, en las cuales se trabajan con la teoría de
lenguajes formales, buscando formalizar al lenguaje en conceptos algebraicos;
y por otra parte la \textit{perspectiva estadística} hace uso de la teoría de
la información, técnicas estadísticas y el aprendizaje automático. En este
trabajo se utilizará el enfoque estadístico basada en redes neuronales
artificiales.

%Por otra parte, para generar un modelo adecuado se necesita seguir un proceso
%que permita definir correctamente cada etapa del presente trabajo y al mismo
%tiempo, que permita evaluar los resultados finales. Debido a lo anterior se
%determinó utilizar la metodoloǵia SEMMA (Sample, Explore, Manipulate, Model and
%Asses), creado por Instituo de sistemas analítico y estadísticos (SAS, por sus
%siglas en inglés).

El presente trabajo se divide en las siguientes secciones: i) antecendentes, ii)
materiales y método, iii) resultados y iv) conclusiones y trabajo futuro.

\section*{Antecedentes}

%SEMMA es un método secuencial para construir modelos de aprendizaje automático
%(\textit{machine learning}) que se conforma de 5 pasos, como se muestra en la
%figura \ref{fig:semma}.

%\begin{figure}[ht!]
  %\centering
  %\includegraphics[width=0.9\linewidth]{semma}
  %\caption{Proceso SEMMA}
  %\label{fig:semma}
%\end{figure}

%La etapa de muestro se refiere a colectar y seleccionar el volumen de los
%conjuntos de datos (datasets) que sirvan para construir el modelo. Básicamente,
%en esta etapa se define las variables independientes (o de salida), así como las
%variables dependientes (o factores). En la etapa de exploración se busca
%comprender las posibles lagunas de los datos así como la relación entre ellos.
%En esta etapa es altamente recomendable utilizar herramientas que permitan
%visualizar los datos de manera gráfica. La siguiente etapa es la modificación
%que consiste en tratar o limpiar las variables que lo requieran, por que la
%salida de esta fase es un dataset limpio que se pasa como entrada del modelo de
%machine learning que se construirá para resolverel problema.
%La penúltima fase es la más importante, el modelo, en esta fase se crear un
%modelo que se adapte a los datos de prueba, puede ocuparse las diversas técnicas
%de minería de datos, en particular, en este trabajo se utilizarán redes
%neuronales artificiales.
%Finalmente, la última fase se conoce como \textit{asssess} o evaluación. Como su
%nombre indica, se evalua el rendimiento del modelo con los datos de prueba (que
%no se usaron en el modelo) para asegurar su confiabilidad y utilidad.

El estudio del procesamiento del lenguaje natural inició desde hace 50 años, sin
embargo, hace tan solo algunos años se volvió a dar un auge en esta rama de la
inteligencia artificial y esto fue debido al resurgimiento de las redes
neuronales artificiales.

Para el estudio del lenguaje se busca tomar datos empíricos para crear un modelo
computacional que represente la lengua. Estos datos empíricos se presentan
dentro de lo que llamamos corpus, un corpus es una recopilación bien organizada
de muestras del lenguaje apartir de materiales escritos o hablados, agrupados
bajo criterios mínimos.

En PLN, la computadora no es capaz de reconocer cuando dos formas de palabra
distintas pertencen a un mismo lexema, por lo se introduce el concepto de
\textit{token}, el cual es la ocurrencia individual de una palabra dentro de un
contexto. Por otra parte, los \textit{tipos} son los diferentes elementos
lingüisticos que existen en un corpus.

El problema de la perspectiva formal es que utiliza gramáticas demasiado
estrictas, lo cual se contrapone al lenguaje natural que tiende a ser bastante
permisivo, por lo cual se introduce una aproximación probabilística.

\subsection*{Modelos de lenguaje}

Un modelo de lenguaje es una tupla \(\mu = (\Sigma, P) \), en donde, \(\Sigma\)
es el vocabulario (o alfabeto) y \(P\) es una medida de probabilidad sobre
\(\Sigma^*\)

Para determinar las probabilidades \(P\) se define un número finito de
probabilidades condicionales, a partir de los cuales se pueden calcular la
probabilidad de cualquier elemento \(\Sigma^*\). De tal forma que si las cadenas
son de la forma \(w^{(1)}w^{(2)} \cdots w^{(T)}\), las probabilidades se obtiene
utilizando la siguiente probabilidad condicional:

\begin{equation}
  p(w^{(1)}w^{(2)} \cdots w^{(T)}) = \prod^{T}_{t=1} p(w^{(t)}|w^{(1)} \cdots
  w^{(t-1)})
  \label{eq:markov}
\end{equation}

La ecuación \ref{eq:markov} se puede ver como un proceso de \textit{Markov de
orden n} y para el cual se puede hacer una aproximación de orden 2, mejor
conocido como \textbf{lenguaje de bigramas} y en el que la probabilidad
simplemente se calcula como:

\begin{equation}
  p(w^{(1)}w^{(2)} \cdots w^{(T)}) \approx \prod^{T}_{t=1} p(w^{(t)}|w^{(t-1)})
  \label{eq:bigramas}
\end{equation}

La ecuación \ref{eq:bigramas} será útil para definir la entradas del
modelo, en el cual, a partir del corpus se definirán pares de palabras
\(w_i,w_j\), sobre las cuales se calcularán las probabilidades. Las
probabilidades de transición puede calcularse por medio de un análisis de
frecuencias de estos bigramas:

\begin{equation}
  p(w_j,w_i) = \dfrac{fr(w_i,w_j)}{fr(w_i)}
  \label{eq:prob-transicion}
\end{equation}

\subsection*{Redes neuronales}

Las \textit{redes neuronales artificiales} (en inglés, ANN, Artificial Neural
Networks) son un modelo simplificado inspirado en el funcionamiento del
cerebro humano para procesar información. Su aplicaciones son muy diversas, pero
para este trabajo representan el modelo a entrenar, por lo cual se definirán los
conceptos básicos que servirán a la hora de definir la arquitectura propuesta.

La unidad mínima de una red neuronal, es la neurona, la cuál cuenta con varias
entradas que se multiplican por cierto pesos \(w\), estás se suman y si la suma
es más grande que cierto umbral \(\theta\), la neurona se activa, en caso
contrario, se encuentra desactivada. Lo anterior se puede resumir mediante la
siguiente expresión:

\[
  u = \sum_{i = 0}^{n} w_ix_i - \theta
\]

Por otra parte, la salida de la neurona es generada por una función de
activación no lineal, la cual puede cambiar, dependiendo de los datos y del
proceso que se quiera realizar, para el presente trabajo se utiliza la función
\textit{softmax}:

\[
  f(u) = \dfrac{e^{u_j}}{\sum^{K}_{k=1} e^{u_k}}
\]


En la figura \ref{fig:neurona}, se aprecia la idea anterior de multiplicar los
pesos $w_i$ por las entradas $x_i$ y si esta suma es mayor que cierto umbral
entonces la neurona se activa.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.9\linewidth]{neurona}
  \caption{Modelo de una neurona artificial.}
  \label{fig:neurona}
\end{figure}

Al conectar las salidas de una neurona a las entradas de otra se forma una red
neuronal. Las unidades de procesamiento se organizan en capas. Hay tres partes
normalmente en una red neuronal: una capa de entrada, con unidades que
representan los campos de entrada, que usualmente se obtiene de los datos
registrados por sensores para el caso de robots o datasets de imágenes para el
caso de visión computacional, y en nuestro caso, bigramas obtenidos del corpus; 
una o varias capas ocultas; y una capa de salida. La figura
\ref{fig:red-neuronal} nos permite entender el concepto de manera gráfica.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.7\linewidth]{red-neuronal}
  \caption{Modelo de una red neuronal artificial.}
  \label{fig:red-neuronal}
\end{figure}

El aprendizaje se logra al comparar los valores de entrada multiplicados por
ciertos pesos que se va ajustando en cada iteración con valores deseados o de
salida. Este proceso se repite muchas veces con el objetivo de que la red
mejore sus predicciones hasta haber alzado un criterio de parada o error
mínimo. Por otra parte, continuamente se presentan a la red ejemplos para los
que se conoce el resultado, y las respuestas que proporciona se comparan con los
resultados conocidos. La información procedente de esta comparación se pasa
hacia atrás a través de la red, cambiando los pesos gradualmente.

\subsection*{Backpropagation}

Para obtener una red neuronal entrenada se requiere la
minimización de una función de riesgo, generalmente el error, el cual
está dado por la comparación entre los valores \textit{deseados} y
los valores \textit{obtenidos} de la red neuronal. Para reducir el error se
deben encontrar los pesos óptimos, por lo cuál se puede utilizar la expresión
\ref{eq:error}

\begin{equation}
  \dfrac{\partial E_{j}^{L}}{\partial w_{ij}^{L}} = 
  -\sum_{k=1}^{K} \delta_{jk}^{L} z_{ik}^{L-1}
  \label{eq:error}
\end{equation}

De la ecuación \ref{eq:error} se entiende que para minimizar el error se obtiene
la parcial con respecto a los pesos, para cada de una de las muestras $K$.
Además $\delta_{k}^{L}$ representa el error modulado de la muestra $k$ en la
capa $L$.  Sin embargo, para la capa $L$ aún no se conocen el error modulado. El
error modulado solo se conoce para la última capa (capa de salida). Por lo cual
se debe buscar una manera de propagar dicho error y es ahí donde se utiliza el
\textbf{algoritmo de backpropagation}.

Este algoritmo se denomina backpropagation o de propagación inversa debido a
que el error se propaga de manera inversa al funcionamiento normal de la red,
de esta forma, el algoritmo encuentra el error en el proceso de aprendizaje
desde las capas más internas hasta llegar a la entrada; con base en el cálculo
de este error se actualizan los pesos de cada capa.
Este algoritmo es un método de aprendizaje que a su vez utiliza otro método de
aprendizaje llamado \textit{gradiente descendente}, por lo que la expresión para
calcula los pesos basados en la iteración anterior se puede escribir como se
observa en la ecuación \ref{eq:pesos}.

\begin{equation}
  w_{ij}^{L}(t+1) = w_{ij}^{L}(t) + \eta
  \sum_{k=1}^{K}\delta_{jk}^{L}z_{ik}^{L-1}
  \label{eq:pesos}
\end{equation}

$w_{ij}^{L}(t+1)$ representan los pesos recalculados para la iteración $t + 1$
a partir de una iteración previa. De dicha iteración $t$ ya se conocen el
error modulado así como las salidas $z$ obtenidas. La figura
\ref{fig:backpropagation} puede ayudar a entender mejor estos conceptos.

\begin{figure}{ht!}
  \centering
  \includegraphics[width=\linewidth]{backpropagation}
  \caption{Modelo de una red neuronal artificial tomando en cuenta la
  \textit{retropropagación}.}
  \label{fig:backpropagation}
\end{figure}

Finalmente, a la ecuación anterior se le agrega un término para que converja
más rápido, comparando los valores de los pesos anteriores y agregando ruido
aleatorio para sacar los pesos de mínimos locales. Por lo que al final la
expresión para encontrar los pesos aplicando backpropagation es:

\begin{equation}
\begin{split}
  w_{ij}^{L}(t+1) = w_{ij}^{L}(t) + \eta
  \sum_{k=1}^{K}\delta_{jk}^{L}z_{ik}^{L-1} + \\
  \mu[w_{ij}^{L}(t) - w_{ij}^{L}(t-1)] + \epsilon_{ij}^{L}(t)
\end{split}
\end{equation}


\section*{Método}

\subsection*{Fuente de datos}

Para la generación del modelo se puede utilizar cualquier corpus en
español\footnote{\footnotesize{Por supuesto que si se utiliza un corpus en
inglés se estará entrenado un modelo del lenguaje en ese idioma.}}, cuentos,
narraciones, revistas, etc. En este caso, se hizo uso de un corpus construido a
partir de un archivo de texto plano, dicho archivo contiene un cuento corto
llamado ``Funes el memorioso'', escrito por Jorge Luis Borges. 
Dicho cuento contiene alrededor de 16370 palabras.

La lectura del archivo de texto plano se realiza por medio del lenguaje de
programación Python para posteriormente darle un procesamiento a los datos
recabados.
%por simplicidad se utiliza el corpus
%provisto por la librería \textbf{NLTK} del lenguaje \textit{python},
%propiamente, el corpus recibe el nombre \texttt{cess\_esp}. Gracias a este
%corpus y a las bibliotecas de python, se puede acceder a varios elementos del
%corpus: palabras, sentencias, parráfos, etc. para este modelo se utiliza el
%corpus dividido en sentencias.

%En la figura \ref{fig:frecuencias}, se puede apreciar las palabras que aparecen
%con más frecuencias en nuestro corpus.

%\begin{figure}[ht!]
  %\centering
  %\includegraphics[width=\linewidth]{grafica-frecuencias}
  %\caption{Frecuencias de algunas palabras del texto}
  %\label{fig:frecuencias}
%\end{figure}

\subsection*{Procesamiento de datos}

Para la construcción del modelo de lenguaje se dividió el corpus de la siguiente
forma:

\begin{itemize}
  \item 70\% para el entrenamiento del modelo \(\mu = ( \Sigma, (A,\Pi) ) \)
  \item 30\% para la evaluación
\end{itemize}

Una vez que se obtuvo la información del archivo de texto plano se
\textit{tokenizo} de tal forma que se obtuvieron arreglos de oraciones, en donde
cada elemento contiene a su vez arreglos de palabras. Después,
para limpiar el corpus basto con eliminar signo de puntuación, de interrogación,
admiración y otros elementos no léxicos. En python se puede hacer mediante el
siguiente código:

\input{snippets/non-lexical}

Adicionalmente, se pueden realizar otras acciones como la eliminación de
\textit{stopwords}(preposiciones, artículos y pronombres que no aportan mucha
información para el análisis). También, se puede realizar un proceso de 
\textit{stemming} con el fin de truncar las palabras y obtener la base
morfológica, sin embargo, esto último no es necesario ni conveniente, ya que al
hacerlo se tendrá que reconstruir las palabras obtenidas una vez estimado el
modelo. 

Finalmente, las entradas de la arquitectura de la red no son las sentencias
procesadas del corpus, sino que en lugar de ello se trabajó con bigramas de la
forma $(w_i,w_j)$, por tal motivo, cada palabra del vocabulario se indexo
utilizando un diccionario y tomando en cuenta etiquetas que permiten identificar
el principio y fin de una oración (BOS, Begin of Sentence; EOS, End Of
Sentence).  Después de obtener los índices se construyeron los bigramas, como se
muestra en la siguiente porción de código.

\input{snippets/bigramas}

Se debe tomar en cuenta que el modelo construido no contiene las probabilidades
de las palabras no vistas en el entrenamiento, por se les debe dar un
tratamiento especial, de no hacerlo, el modelo no podrá adaptarse a nuevas
palabras. Por lo anterior, se sustituyen los elementos \textit{hapax}(palabras
cuya frecuencia es igual a 1) en el corpus de entrenamiento por el símbolo de
``fuera de vocabulario'' (OOV, por sus siglas en inglés). En python esto se
logra de la siguiente forma:

\input{snippets/hapax}

\subsection*{Arquitectura}

Para estimar un modelo \(\mu = (\Sigma,(A,\Pi)) \), en donde \(\Sigma\) es un
vocabulario, A son las probabilidades de transición y \(\Pi\) son las
probabilidades iniciales, se propone la arquitectura mostrada en la figura
\ref{fig:arquitectura}.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.7\linewidth]{arquitectura}
  \caption{Arquitectura para estimar el modelo}
  \label{fig:arquitectura}
\end{figure}

Como se observa en la figura \ref{fig:arquitectura}, la arquitectura propuesta
consta de 3 capas, las cuales, vistas de abajo hacia arriba son:

\begin{itemize}
  \item Capa de embedding
  \item Capa oculta
  \item Capa de salida
\end{itemize}

El vector de entradas de la red neuronal
serán vectores \textit{one-hot}, generados apartir de los indices de las
palabras \(w_i\) en el vocabulario, que a su vez se multiplican por una matriz
\(C\) tal que $C \in \mathbb{R}^{d \times N}$, donde $N$ es el tamaño del
vocabulario ($\Sigma$) y $d$ es un hiperparámetro de estas representaciones.

La siguiente capa de la red (capa oculta) nos permitirá obtener las
probabilidades y se define como se observa en la ecuación \ref{eq:hidden}

\begin{equation}
  h(i) := \tanh(WC(i) + b)
  \label{eq:hidden}
\end{equation}

En donde $W \in \mathbb{R}^{m\times d}$ y $b \in \mathbb{R}^m$, con $m$ como el
número de unidades ocultas.

Finalmente, la capa de salida tiene una función activación $a(i)$,  dada por la
ecuación mostrada en \ref{eq:activacion}.

\begin{equation}
  a(i) := Uh(i) + c
  \label{eq:activacion}
\end{equation}

En donde $i$ es el índice de la palabra de entrada, $U \in \mathbb{R}^{N \times
m}$ y $c \in \mathbb{R}^N$. $a(i)$ es un vector, cuyas entradas $a_k(i)$ indexan
a la palabra subsiguiente que el modelo intenta estimar, llamado $W_k$, por lo
tanto la activacion puede verse como:

\begin{equation}
  p(w_j|w_i) := \dfrac{e^{a_j(i)}}{\sum^{K}_{k=1} e^{a_k(i)}}
  \label{eq:activacion-2}
\end{equation}

Es decir, la ecuación \ref{eq:activacion-2} es una función \textit{Softmax} que
se encuentra en función de el elemento \textit{iesímo}.

Por último se necesita una función de riesgo, que nos permitirá ajustar
todos los pesos de la red. No se habla como tal de una función de error, dado
que no se estan comparando valores, sino que en lugar de eso se analizan los
pares de vectores $w_i$, $w_j$, mejor conocidos como bigramas, debido a ello,
la función de riesgo se calcula 
por medio de la entropía cruzada y se define como:

\begin{equation}
  R(\theta) = - \sum_i \sum_k y_k \ln p(w_k|w_i)
  \label{eq:riesgo}
\end{equation}

La implementación del modelo se realizó en \textit{Python} por ser el lenguaje
más ocupado para cuestiones relacionadas con \textit{machine learning}. 
Para hacer las operaciones solicitadas por la red neuronal se utilizó
\textit{numpy}, una biblioteca bastante ocupada para cálculos matemáticos.

Como ya se mencionó anteriormente, la entrada de la red neuronal serán bigramas
formados a partir juntar los índices de las las palabras del vocabulario en
pares. De tal manera que, un
bigrama es de la forma:

\begin{equation}
  S = {(i,j): (w_i,w_j)\; \text{es un bigrama}}
  \label{eq:bigrama}
\end{equation}

De esta forma, si la entrada de es de tamaño $N$, la salida será de ese mismo
tamaño, dado que intentamos predecir la palabra siguiente. Los hiperparámetros
que se ocuparon son: para la primera capa, \textit{100} unidades; y para la capa
oculta se utilizaron \textit{300} unidades. A continuación se presenta la
implementación del modelo en python

\input{snippets/red-neuronal}

\subsection*{Evaluación}

Para evaluar el modelo no se puede hacer una comparación de
``valor esperado contra valor obtenido'', debido a que no se esta entrando un
red de clasificación, sino que se trabajan con pares de palabras, por lo tanto,
para medir la efectividad de nuestro modelo se utilizará la entropía empírica,
la cual se calcula de la siguiente forma:

\begin{equation}
  H_E(P) = - \dfrac{1}{T}\sum_{t=1}{T} \log_2 
  p(w^{(t)}| w^{(t-n+1)}\cdots w^{(t-1)})
  \label{eq:entropia}
\end{equation}

Sin embargo, en la industria es más común utilizar la \textit{perplejidad}, que
se define con base en la entropía como se observa en la ecuación
\ref{eq:perplejidad}

\begin{equation}
  P_x(\mu) = 2^{H_E(\mu)}
  \label{eq:perplejidad}
\end{equation}

Los valores obtenidos fueron:
\begin{itemize}
  \item Entropía promedio: 4.71
  \item Perplejidad total: 26.25
\end{itemize}

Así mismo al evaluar oraciones no vistas en el entrenamiento, los resultados
fueron los siguientes:

\begin{figure}[ht!]
  %\centering
  \includegraphics[width=0.9\linewidth]{prob-cadenas}
\end{figure}

\section*{Resultados}

El entrenamiento de la red neuronal utilizando el corpus ``funes el memorioso''
se realizó en \textbf{1 minuto con 9 segundos}, un tiempo bastante aceptable
para la cantidad de iteraciones y el valor de convergenca utilizados y así como
otros parámetros e hiperparámetros que se muestran a continuación:

\begin{itemize}
  \item Número de tipos: 873
  \item Número de tokens: 2244
  \item Núm de iteraciones: 50
  \item Valor de convergencia ($\eta$): 0.5
  \item N = 2332
  \item d = 100
  \item m = 300
\end{itemize}

La ejecución del algoritmo se realizó en una laptop Dell Inspiron 7559 con un
procesador core i7 de sexta generación, como 8 núcleos;16 GB de ram y una GPU
GeForce GTX 960M de 4GB, dichas características se puede apreciar en la
figura \ref{fig:monitor}

\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.9\linewidth]{monitor}
  \caption{Características del equipo en donde se entrenó la red neuronal.}
  \label{fig:monitor}
\end{figure}

Como se puede observar en la sección anterior, al utilizar una red neuronal de 3
capas, que tuvo como parámetros 50 iteraciones, dimensión ($d$) igual a 100 y
300 capas ocultas, se obtuvo una \textit{entropía promedio} de 4.71, dicho valor
se considera adecuado, ya que por definición la entropía se puede entender como
el inverso de las probabilidades que el modelo intenta estimar, es decir, se
entiende que, para el caso de la probabilidad, entre más cerca se encuentre del
valor 1 mejor será el modelo, por el contrario, para la entropía, entre más bajo
sea el valor se considera mejor, ya que representa la cantidad de información
que el modelo aporta en sí. 

Para evitar confusiones, es necesario aclarar que es casi
imposible reducir la medida de la entropía a un número menor que 1, ya que esto
implica que se tiene un corpus de entrenamiento que prácticamente contiene todas
las palabras del idioma. Por otra parte, la perplejidad simplemente ajustar los
entropía a una escala exponencial, en este caso, se obtuvo un valor de 26.25,
siendo un buen valor para la cantidad de token y tipos utilizados.

Finalmente, en la figura 6, se utilizó el algoritmo de PCA (Principal Components
Analysis, por sus siglas en inglés) para ver gráficamente la relación entre
los vectores palabra observados en el corpus de entrenamiento.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.9\linewidth]{pca}
  \caption{PCA sobre los vectores palabra del corpus de entrenamiento.}
  \label{fig:monitor}
\end{figure}

\section*{Conclusiones y trabajo futuro}

Se adquirió un corpus en español, cuyo tamaño fue suficiente para tener un
número de tipos y tokens  adecuado para generar un modelo del lenguaje con una
baja entropía con se observa en las secciones correspondientes a la evaluación y
el análisis de los resultados.
Así mismo, se procesó adecuadamente el corpus a eliminar elementos no léxicos
del corpus original, esto permitió, que en la etapa de evaluación se puedan
generar oraciones con mejores probabilidades. Del mismo modo, se observa que la
división de datos de entrenamiento y prueba fue adecuado, ya que al tomarse el
70\% de los datos totales para la fase de entrenamiento demostró ser un marco
reprepresentativo de todos los \textit{tipos} que contenía el texto original.

Por otra parte, para la arquitectura de 3 capas en la red neuronal \textit{feed
foward}, se observa que los parámetros encontrados son óptimos ya que permiten
obtener tiempos de entrenamiento pequeños y un vector de probabilidades
alto,asociado a las palabras subsecuentes.

Finalmente, en la evaluación del modelo se demostró un valor de entropía bajo y
por lo tanto el valor de la información que el modelo aporta se considera
óptimo. En conclusión el modelo del lenguaje basado en bigramas resulta ser una
buena aproximación para la generación de aplicaciones de PLN. 

Como trabajo futuro se considera víable construir un modelo de
\textit{n-gramas} cuya entropía tenderá a ser menor y por ende se obtendrán
mejores probabilidades de ciertas oraciones no vistas en el entrenamiento.

Para dicha implementación del modelo de \textit{n-gramas}, se tomará como base
la arquitectura mostrada en la figura \ref{fig:n-gramas}. Dicho
modelo se considera como una generalización que permite que la arquitectura neuronal
considere varias entradas previas, es decir, varias palabras precedentes, que
den lugar a la probabilidad de la palabra siguiente.

\begin{figure}[ht!]
  \centering
  \includegraphics[width=0.8\linewidth]{n-gramas}
  \caption{Modelo de n-gramas}
  \label{fig:n-gramas}
\end{figure}

Al generar un modelo de embedding, se asume que se pueden representar las
cadenas de palabras como el promedio de los embedding que los conforman, esto
es, si tenemos una cadena $w_{i1}, w_{i2},\cdots,w_{i_T}$, su embedding será:

\begin{equation}
  C(i_1,i_2,\cdots,i_T) = \dfrac{1}{T} \sum_{t=1}{T}C(i_t)
  \label{eq:prom-embedding}
\end{equation}

Por lo que apartir de la ecuación \ref{eq:prom-embedding}, se puede tomar este
embedding para correr la red sobre él y obtener la probabilidad como:

\begin{equation}
  p(w_j|w_i,w_{i2},\cdots,w_{i_T}) = \phi(w_{i1},w_{i2},\cdots,w_{i_T})
  \label{eq:prob-gen}
\end{equation}

En donde $\phi(w_{i1},w_{i2},\cdots,w_{i_T})$ es la red aplicada a estas
palabras previas.

\begin{thebibliography}{9}
  \bibitem{bengio} Y. Bengo, R. Ducharme, V. Pascal, J. Christian. \textit{A
    NeurNeural Probabilistic Language Model}. Journal of Machine Learning
    Research (2003) 1137-1155.
  \bibitem{ibm} IBM Cloud Education. \textit{Natural Language Processing(NLP)}.
    Tomado de \url{https://www.ibm.com/cloud/learn/natural-language-processing}
  \bibitem{jason} J. Browniee. \textit{What is natural language processing}.
    Tomado de
    \url{https://www.machinelearningmastery.com/natural-language-processing/}
  \bibitem{savage} J. Savage. \textit{Introducción a las redes neuronales}.
    Tomado de \url{https://biorobotics.fi-p.unam.mx/wp-content/uploads/Courses/
    reconocimiento_de_patrones/2021-2/lecciones/
    leccion12_reconocimiento_de_patrones_2021-2.pdf}
  \bibitem{ibm2} IBM. \textit{El modelo de redes neuronales}. Tomado de
    \url{https://www.ibm.com/docs/es/spss-modeler/SaaS?topic=networks-neuronal-model}
  \bibitem{jason} J. Browniee. \textit{Gentle introduction to statistical
    language modeling and neuronal language models} Tomado de 
    \url{https://machinelearningmastery.com/statistical-language-modeling\\
    -and-neural-language-models/}
  \bibitem{kurt} K. Hornik, M. Stinchcombe, H. White. \textit{Multilayer
    feedforward networks are universal approximators}. Neural Networks, 1989.
  \bibitem{shai} S. Shalev-Shwartz, S. Ben-David. \textit{Understanding machine
    learning: From theor to algorithms}. Cambridge university press. 2004.
  \bibitem{piotr} P. Bojanowsky, E. Grave, A. Joulin, T. Mikolov.
    \textit{Enriching word vectors with subwords information}. Transactions of
    Association from Computer Linguistics, 2017.
  \bibitem{tomas} T. Mikolov, I. Sutskever, K. Chen, G. Corrado, J. Dean.
    \textit{Distributed respresentations of words and phrases and their
    compositionality}. In Advances in neural information processing systems,
    2013.
\end{thebibliography}

\end{document}
