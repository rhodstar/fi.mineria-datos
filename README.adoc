= Generación de un modelo de lenguaje a través del proceso SEMMA
Creado por Rodrigo Francisco <rhodfra@gmail.com>
Version 1.0, 03.08.2021
:toc: 
:toc-placement!:
:toclevels: 4                                          
:toc-title: Contenido

// Ruta base de las imagenes
:imagesdir: ./README.assets/ 

// Resaltar sintaxis
:source-highlighter: pygments

// Iconos para entorno local
ifndef::env-github[:icons: font]

// Iconos para entorno github
ifdef::env-github[]
:caution-caption: :fire:
:important-caption: :exclamation:
:note-caption: :paperclip:
:tip-caption: :bulb:
:warning-caption: :warning:
endif::[]

Proyecto de investigación para la materia de Mineria de datos.

image::peek.gif[]
